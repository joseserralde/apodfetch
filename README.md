![](https://i.imgur.com/rRhskyp.png)

# apodfetch
apodfetch is a simple script that get the Astronomy Picture of the Day (APOD) from NASA API in a terminal using RGB ANSI codes and unicode block graphics characters.

## Getting started :satellite:

 1. `git clone https://github.com/beamop/apodfetch.git`
 2. `cd apodfetch/`
 3. `sudo chmod a+x install.sh`
 4. `Update apiKey variable`
 5. `./install.sh`

Now you can execute `apodfetch` in your terminal :smiley:

## Thanks to

 1. Stefan Haustein ([TerminalImageViewer](https://github.com/stefanhaustein/TerminalImageViewer))
